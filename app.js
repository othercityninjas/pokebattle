// The submission should take two Pokémon and determine which one has the favourable type advantage.
// In the case of no type advantage the “winner” should be the Pokémon with the highest base stats.
// In the case of a tie the “winner” should be the first Pokémon passed to it.


const app = {};


app.getPokemon = (identifier) => {
    return $.ajax({
        url: 'http://proxy.hackeryou.com',
        method: 'GET',
        dataType: 'json',
        data: {
            reqUrl: `https://pokeapi.co/api/v2/pokemon/${identifier}`,
        }
    });
};

app.getType = (endpoint) => {
    return $.ajax({
        url: 'http://proxy.hackeryou.com',
        method: 'GET',
        dataType: 'json',
        data: {
            reqUrl: endpoint,
        }
    });
};


app.reset = () => {
    $('#battle').on('click', () => {
        $('main').toggleClass('hide');
        $('.opponents').empty();
        $('form').toggleClass('hide');
        $('#battle').off();
    })
}

app.displayWinner = (winner, loser) => {
    $('#battle').on('click', () => {
        $(loser).css('opacity', 0.25);
        $('p').css('opacity', 0.25);
        $(winner).append('<p class="badge">Winner!</p>');
        $(`${winner} > img`).css('background-color', 'white');
        $(`${winner} > figcaption`).css({
            'background-color': 'goldenrod',
            'transform': 'rotate(5deg)',
            'font-weight': '900',
            'bottom': '2%'
        });
        $('#battle').off().text('Play again?');
        app.reset();
    });
}

app.displayBattle = (hero, adversary) => {
    $('main').toggleClass('hide');
    const heroProfile = `<figure class="hero">
                            <img src="${hero.sprites.front_default}">
                            <figcaption>${hero.name}</figcaption>
                        </figure>`

    const adversaryProfile = `<figure class="adversary">
                                <img src="${adversary.sprites.front_default}">
                                <figcaption>${adversary.name}</figcaption>
                            </figure>`

    $('.opponents').append(heroProfile);
    $('.opponents').append('<p>vs</p>');
    $('.opponents').append(adversaryProfile);
}

app.hideForm = () => {
    $('form').toggleClass('hide');
    $('input').val('');
}


app.simplifyArr = (arr) => {
    return arr.map(each => each.name);
} 

app.calculateTotalDamage = (damages, adversaryTypes) => {
    let heroPoints = 0;
    let adversaryPoints = 0;

    const doubleDamageTo = app.simplifyArr(damages.double_damage_to);
    const halfDamageTo = app.simplifyArr(damages.half_damage_to);
    const zeroDamageTo = app.simplifyArr(damages.no_damage_to);

    const doubleDamageFrom = app.simplifyArr(damages.double_damage_from);
    const halfDamageFrom = app.simplifyArr(damages.half_damage_from);
    const zeroDamageFrom = app.simplifyArr(damages.no_damage_from);

    adversaryTypes.forEach(oType => {
        if (doubleDamageTo.indexOf(oType) > -1) {
            heroPoints += 2;
        }
        else if (halfDamageTo.indexOf(oType) > -1) {
            heroPoints += 0.5;
        }
        else if (zeroDamageTo.indexOf(oType) > -1) {
            // add nothing
        }
        else {
            heroPoints += 1;
        }

        if (doubleDamageFrom.indexOf(oType) > -1) {
            adversaryPoints += 2;
        }
        else if (halfDamageFrom.indexOf(oType) > -1) {
            adversaryPoints += 0.5;
        }
        else if (zeroDamageFrom.indexOf(oType) > -1) {
            // add nothing
        } else {
            adversaryPoints += 1;
        }
    });

    const advantage = heroPoints - adversaryPoints;
    return advantage;
}

app.calculateBaseStats = (hero, adversary) => {
    const heroStats = hero.stats.reduce((acc, curr) => ({ base_stat: acc.base_stat + curr.base_stat }));
    const adversaryStats = adversary.stats.reduce((acc, curr) => ({ base_stat: acc.base_stat + curr.base_stat }));
    if (adversaryStats.base_stat > heroStats.base_stat) {
        app.displayWinner('.adversary', '.hero');
    } else {
        app.displayWinner('.hero', '.adversary');
    }
}

app.determineWinner = (hero, adversary, final) => {
    if (final > 0) {
        app.displayWinner('.hero', '.adversary');
    } else if (final < 0) {
        app.displayWinner('.adversary', '.hero');
    } else {
        app.calculateBaseStats(hero, adversary);
    }
}


app.battle = (hero, adversary) => {
    app.displayBattle(hero, adversary);

    const adversaryTypes = adversary.types.map(each => each.type.name);

    const typePromises = [];
    hero.types.forEach(each => {
        typePromises.push(app.getType(each.type.url));
    })

    Promise.all(typePromises).then((res) => {
        const battle = res.map(each => app.calculateTotalDamage(each.damage_relations, adversaryTypes));
        const final = battle.reduce((acc, curr) => acc + curr);
        app.determineWinner(hero, adversary, final);
    })
}

app.getBothPokemon = (first, second) => {
    const hero = app.getPokemon(first);
    const adversary = app.getPokemon(second);
    
    Promise.all([hero, adversary]).then((res) => {
        app.battle(res[0], res[1]);
    });
}

app.init = () => {
    $('form').on('submit', (e) => {
        e.preventDefault();
        const firstPokemon = $('#firstPokemon').val().toLowerCase().trim();
        const secondPokemon = $('#secondPokemon').val().toLowerCase().trim();
        if(firstPokemon && secondPokemon) {
            app.getBothPokemon(firstPokemon, secondPokemon);
            app.hideForm();
        }
    });
}

$(function(){
    app.init();
});