ASSUMPTIONS
- Users are familiar with Pokemon and can spell names of chosen Pokemon correctly, so promises will always be resolved/API will always return data
- API calls are unlimited, so performance and optimization are not prioritized (e.g., storing data in cache is not necessary)

DEPENDENCIES
- PokeAPI (external)
- minified jQuery (internal)

Users simply inputs their choice Pokemon, and when submitted, the page displays their choices before they can click a 'Battle!' button to determine the winner based solely on advantages of type or, failing that, on individual base stats. Alternatively, the results could be displayed immediately upon form submission, but adding a pause where users can see their chosen Pokemon not only builds some anticipation (even if so little) but allows room for future additions like displaying stats and offering users the option to change their choice.

If I were to revisit this challenge, I would like to make this web app more accessible and more inviting to users who are unfamiliar with Pokemon. Adding autocomplete or a drop down menu would take the guesswork out of spelling (I had forgotten there are double Ts in both Diglett and Rattata). Also, refactoring this using React or a similar framework that would allow data/images to display instantly and more dynamically, removing the need for users to click a button.

I chose to keep the design of this web app quite simple to reflect the simple and small scope of the assignment as well as to focus on properly navigating the different endpoints and the nested data of the API. (I also didn't want to wait too long before sending this your way.) This took roughly 13 hours to build over the weekend. About 90% of the time was spent on Javascript, familiarizing myself with the API (and more generally with Pokemon), and designing a system that takes into consideration all damage relations based on type. Though the code can always be improved, hopefully what I have here is manageable, easy to read, and extensible in the future if/when any of the assumptions change.